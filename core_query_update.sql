SELECT
    ff.term_id,
    ff.name,
    ff.slug,
    ff.description,
    ff.post_date
FROM
    (
	    SELECT
	        p.*,
	        bbb.*
	    FROM
	        `dogtober_posts` AS p
	    LEFT JOIN(
		        SELECT a.*,
		            b.object_id,
		            c.name,
		            c.slug
		        FROM
		            `dogtober_term_taxonomy` AS a
		        INNER JOIN `dogtober_term_relationships` AS b
		        ON
		            a.term_id = b.term_taxonomy_id
		        INNER JOIN `dogtober_terms` AS c
		        ON
		            a.term_id = c.term_id
		        WHERE
		            a.parent != 0
		    ) AS bbb
		ON
		    bbb.object_id = p.ID
		WHERE
		    p.post_status = "publish" AND p.post_type = "post"
		ORDER BY
		    p.post_date
		DESC
	) AS ff
GROUP BY
    ff.term_id
ORDER BY
    ff.post_date DESC
LIMIT 8