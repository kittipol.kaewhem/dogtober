<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dogtober' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7h&J)`G$5><h^2r7G%Y&dO*%yZej5(94c|gk86<lKH:Z}ciG7ixB@p2YwxAzP*&j' );
define( 'SECURE_AUTH_KEY',  'OiA(g5G_i}/2p_(,q,;+Xe@&=3 6d[I]S|}5@F4Gf~pXnj6&]j_f x:x?dwLc-~.' );
define( 'LOGGED_IN_KEY',    '*U9U=fj l5rA!!b14fG`ZJ+ZD}UU|Q+.osv*G|T><*GoA^Nj]/=tIB:6hl8!#0 U' );
define( 'NONCE_KEY',        ' o9ec`(5S,$=!L!D$}nqfGA3qDjcWzS~16MNXHK)K]~7$TekNOJa6=Nw.),GP;pV' );
define( 'AUTH_SALT',        'A+I@/FN${Uh+wmsZ`&^M0..n@,3}0o ($_NjP%%jXx]O4|J(/do41TdP$/)a[71*' );
define( 'SECURE_AUTH_SALT', 'Yh5Ms}3[awgyHX!;nceZPNw+2A{[h6&k])?M<?lL2M~e_yc`ba!|nO!PGRG{JK$V' );
define( 'LOGGED_IN_SALT',   'E4w%kGF`)A]0.t@`|94,)2n^.HoSk0/*~D/*WyXV*^46B&X[KTsyIsfP+DTQ}vxn' );
define( 'NONCE_SALT',       '_z/Df(H~.L%)5&PXy^AN|`(U.i:IJLrx<Oac<oaHsS*bQbL9tT(wEuM}L)ok:f|n' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'dogtober_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
