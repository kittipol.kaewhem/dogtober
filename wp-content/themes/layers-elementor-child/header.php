<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<?php wp_head(); ?>
	<script>
		var base_url = '<?php echo get_site_url() ; ?>/';
	</script>
</head>
<?php
	global $active_menu_cc,$arr_site,$post;
    
	$site_menu='';

    $arr_site = array(
      "home"=>"HOME",
      "learn"=>"LEARN",
      "journey"=>"JOURNEY",
       "experience"=>"EXPERIENCE"
    );
	foreach ($arr_site as $key => $value) {
		$active = "";
		if($active_menu_cc==$key){
			$active="active";
		}else{
            if($post->post_name=="home" && $key==$post->post_name){
                $active="active";
            }
        }
        $cc = get_category_by_slug( $key );
        $list_child = get_categories(
            array( 
                'parent' => $cc->cat_ID,
                'orderby'=>'term_id',
                'order'=>'asc',
                'hide_empty' => false
            )
        );
        $link = get_site_url().'/listing/?page='.$key;
        if($key=="home"){
            $link = get_site_url();
        }
        // if($key != "home" && count($list_child)>0){
        //         $sub='<ul class="sub-menu">';
        //         foreach ($list_child as $k => $v) {
        //             $ev = get_object_vars($v);
        //             $sub.='<li class="menu-item"><a href="'.get_site_url().'/listing/?page='.$ev["slug"].'">'.$ev["name"].'</a></li>';
        //         }
        //         $sub.='</ul>';
        //         $site_menu.='<li class="'.$active.' menu-item menu-item-has-children '.$key.'"><a href="'.get_site_url().'/listing/?page='.$key.'">'.$value.'</a>'.$sub.'</li>';
        // }else{
            $site_menu.='<li class="'.$active.' menu-item '.$key.'"><a href="'.$link.'">'.$value.'</a></li>';
        // }
	};
?>
<body <?php body_class(); ?>>
    <div class="wrapper invert off-canvas-right" id="off-canvas-right">
        <a class="close-canvas" data-toggle="#off-canvas-right" data-toggle-class="open">
            <i class="l-close"></i> Close </a>
        <div class="content nav-mobile clearfix">
            <nav class="nav nav-vertical">
                <ul id="menu-header-menu" class="menu">
                    <?=$site_menu?>
                </ul>
            </nav>
        </div>
        <aside  id="search-2" class="content widget widget_search">
            <form role="search" method="get" class="search-form" action="<?php echo get_site_url() ; ?>">
                <label>
                    <span class="screen-reader-text">ค้นหาสำหรับ:</span>
                    <input class="cs-field2" placeholder="ค้นหา . . ." type="text">
                </label>
                <div class="cs-submit">ค้นหา</div>
            </form>
        </aside>
    </div>
    <div class="wrapper-site">
        <section class="block_header">
            <div class="header_space"></div>
            <div class="header_navbar">
                <div class=" header-block">
                    <div class="logo">
                        <div class="site-description">
                            <h3 class="sitename sitetitle"><a href="http://localhost/dogtober"></a></h3>
                            <p class="tagline"></p>
                        </div>
                    </div>

                    <nav class="nav nav-horizontal">

                        <ul id="menu-header-menu-1" class="menu">
                            <?=$site_menu?>
                        </ul>
                        <div  class="header-search">
                            <div class="cs-sign"><i class="fas fa-search"></i></div>
                        </div>
                        <div class="hide-xs body-search custom-search">
                            <input class="cs-field" type="text" placeholder="ค้นหา . . .">
                            <span class="cs-close"><i class="fas fa-times"></i></span>
                        </div>
                        <a class="responsive-nav" data-toggle="#off-canvas-right" data-toggle-class="open">
                            <span class="l-menu"></span>
                        </a>
                    </nav>
                </div>
            </div>
        </section>

        <section id="wrapper-content" class="wrapper-content">