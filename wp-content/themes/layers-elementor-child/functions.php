<?php
date_default_timezone_set("Asia/Bangkok");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
// error_reporting(E_ALL); // Show all errors & warning
error_reporting(E_ERROR | E_PARSE); // Show only errors


add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'wp_enqueue_scripts', 'customSite',99 );
function customSite(){
	wp_enqueue_style( 'fontawesome', get_site_url() . '/css/fontawesome.all.min.css', array(), '', 'all');
  wp_enqueue_style( 'customsite', get_site_url() . '/css/customsite.css', array(), '', 'all');
	wp_enqueue_script( 'fontawesome', get_site_url() . '/js/fontawesome.all.min.js',array ( 'jquery' ),'', true);
  wp_enqueue_script( 'script', get_site_url() . '/js/customsite.js',array ( 'jquery' ),'', true);
}

function my_scroll() {
	wp_enqueue_script( 'my_scroll', get_site_url() . '/js/jquery.nicescroll.js',array ( 'jquery' ),'', true);
}
add_action('wp_enqueue_scripts', 'my_scroll');

function TrimWithDot($data /*String*/, $len /*Integer*/ = 100 , /*String */ $end = "..."){
	return mb_strimwidth($data, 0, $len, $end);
}

function helloStranger(){
	echo '<h1 style="text-align:center;margin-top:40vh;">Hello Stranger ;)</h1>';
	return false;
}
function prePath($path){
	$tr = array(
		"./"=>get_site_url()."/",
		"../"=>get_site_url()."/",
		'"'=>"",
		"'"=>""
	);
	return strtr($path,$tr);
}

// add custom interval
function cron_add_minute( $schedules ) {
	// Adds once every minute to the existing schedules.
    $schedules['everyminute'] = array(
	    'interval' => 60,
	    'display' => 'Once Every Minute' 
    );
    return $schedules;
}
add_filter( 'cron_schedules', 'cron_add_minute' );

function extractVars($item){
	return get_object_vars($item)[0];
}

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;

            
        }
    }
    return false;
}

function buildArrCortext($layout,$arr,$parent_cate = array()){
  $arr_result = array();
  if($layout == "search"){
    foreach ($arr as $k => $v) {
      $ev = get_object_vars($v);
      $all_cate = get_the_category($ev["ID"]);
      $this_cate = array();
      foreach ($all_cate as $key => $value) {
        $evalue = get_object_vars($value);
        if($evalue["category_parent"]== 0 && $evalue["term_id"]!=1){
          $this_cate = $evalue;
        }
      }
      $arr_result[] = array(
        "id" => $ev["ID"],
        "slug" => $ev["post_name"],
        "name" => $ev["post_title"],
        "description" => $ev["post_excerpt"],
        "img"=> get_the_post_thumbnail_url( $ev["ID"], 'thumbnail' ),
        "parent_name" => isset($this_cate["name"])?$this_cate["name"]:"",
        "parent_slug" => isset($this_cate["slug"])?$this_cate["slug"]:"",
        "parent_id" => isset($this_cate["term_id"])?$this_cate["term_id"]:""
      );
    }
  }else if($layout == "home"){
    foreach ($arr as $k => $v) {
      $ev = get_object_vars($v);
      $parent_cate = get_object_vars(get_term_by("slug",$ev["slug"],"category"));
      $arr_result[] = array(
        "id" => $ev["term_id"],
        "slug" => $ev["slug"],
        "name" => $ev["name"],
        "description" => $ev["description"],
        "img"=> z_taxonomy_image_url($ev["term_id"],'category')?z_taxonomy_image_url($ev["term_id"],'category'):"",
        "parent_name" => isset($parent_cate["name"])?$parent_cate["name"]:"",
        "parent_slug" => isset($parent_cate["slug"])?$parent_cate["slug"]:"",
        "parent_id" => isset($parent_cate["term_id"])?$parent_cate["term_id"]:""
      );
    }
  }else if($layout == "book"){
    foreach ($arr as $k => $v) {
      $ev = get_object_vars($v);
      if($ev["parent"] == $parent_cate["term_id"]){
        $arr_result[] = array(
          "id" => $ev["term_id"],
          "slug" => $ev["slug"],
          "name" => $ev["name"],
          "description" => $ev["description"],
          "img"=> z_taxonomy_image_url($ev["term_id"],'category')?z_taxonomy_image_url($ev["term_id"],'category'):"",
          "parent_name" => isset($parent_cate["name"])?$parent_cate["name"]:"",
          "parent_slug" => isset($parent_cate["slug"])?$parent_cate["slug"]:"",
          "parent_id" => isset($parent_cate["term_id"])?$parent_cate["term_id"]:""
        );
      }
    }
  }else if($layout == "index"){
    foreach ($arr as $k => $v) {
      $ev = get_object_vars($v);
      $arr_result[] = array(
        "id" => $ev["ID"],
        "slug" => $ev["post_name"],
        "name" => $ev["post_title"],
        "description" => $ev["post_excerpt"],
        "img"=> get_the_post_thumbnail_url( $ev["ID"], 'thumbnail' ),
        "parent_name" => isset($parent_cate["name"])?$parent_cate["name"]:"",
        "parent_slug" => isset($parent_cate["slug"])?$parent_cate["slug"]:"",
        "parent_id" => isset($parent_cate["term_id"])?$parent_cate["term_id"]:""
      );
    }
  }else{

  }
  return $arr_result;
}

function renderHTML($txt="",$search = false){
  // Config Variables
  $template_layout = "";
  $parent_cate = array();
  $arr_result = array();

  $res_html = "";


  $ppquery="";
  if($search){
    $template_layout = "search";
    $ppp = new WP_Query( array( 
      's' => $txt,
      'post_type' => 'post',
      'orderby'=>'post_date',
      'order'=>'desc',
      'posts_per_page'=>"-1",
    ) );

    $arr_result = buildArrCortext($template_layout,$ppp->posts);
    $n_cs = count($arr_result);

    $res_html.='<div class="cs-topic">
          ผลการค้นหา : "'.$txt.'" ทั้งหมด '.$n_cs.' รายการ
        </div>';
  }else{
    if($txt==""){
      global $wpdb;

      $template_layout = "home";
      // Old Query
      // $sql = '
      //   SELECT
      //       *
      //   FROM
      //       `dogtober_posts`
      //   WHERE
      //       post_status = "publish" and post_type = "post"
      //   ORDER BY
      //       post_date
      //   DESC
      //   LIMIT 8
      // ';

      
      $sql = '
        SELECT
            ff.term_id,
            ff.name,
            ff.slug,
            ff.description,
            ff.post_date
        FROM
            (
              SELECT
                  p.*,
                  bbb.*
              FROM
                  `dogtober_posts` AS p
              LEFT JOIN(
                    SELECT a.*,
                        b.object_id,
                        c.name,
                        c.slug
                    FROM
                        `dogtober_term_taxonomy` AS a
                    INNER JOIN `dogtober_term_relationships` AS b
                    ON
                        a.term_id = b.term_taxonomy_id
                    INNER JOIN `dogtober_terms` AS c
                    ON
                        a.term_id = c.term_id
                    WHERE
                        a.parent != 0
                ) AS bbb
            ON
                bbb.object_id = p.ID
            WHERE
                p.post_status = "publish" AND p.post_type = "post"
            ORDER BY
                p.post_date
            DESC
          ) AS ff
        GROUP BY
            ff.term_id
        ORDER BY
            ff.post_date DESC
        LIMIT 8
      ';

      $arr_result = buildArrCortext($template_layout,$wpdb->get_results($sql));
    }else{
      $parent_cate = get_object_vars(get_term_by("slug",$txt,"category"));
      $parent_cate["img"] = z_taxonomy_image_url($parent_cate["term_id"],'category')?z_taxonomy_image_url($parent_cate["term_id"],'category'):"";

      $child_categories = array();

      if($parent_cate["parent"]==0){
        $template_layout = "book";

         $args = array(
        'type'                     => 'post',
        'child_of'                 => $parent_cate["term_id"],
        'orderby'                  => 'term_id',
        'order'                    => 'ASC',
        'hide_empty'               => FALSE,
        'hierarchical'             => 1,
        'taxonomy'                 => 'category',
        ); 
        $child_categories = get_categories($args);
      }else{
        $template_layout = "index";

        $args = array(
        'category'         => $parent_cate["term_id"],
        'numberposts'      => '-1',
        'orderby'          => 'post_date',
        'order'            => 'ASC',
        ); 
        $child_categories = get_posts($args);

        $temp_big_parent = get_object_vars(get_term($parent_cate["parent"]));

        $parent_cate["parent_name"]= $temp_big_parent["name"];
        $parent_cate["parent_slug"]= $temp_big_parent["slug"];
      }
      $arr_result = buildArrCortext($template_layout,$child_categories,$parent_cate);
    }
  }

  // เวลาเช็คให้ var_dump 3 ตัวนี้เช็คก่อน
  // var_dump($template_layout,$parent_cate,$arr_result);
  // var_dump($parent_cate);

  if ($template_layout == "index") {
    $lef_html.='<div class="subject">'.$parent_cate["name"].'</div>
                      <div class="block_category">
                        <ul>
                          <li><a href="'.get_site_url().'/listing/?page='.$parent_cate["parent_slug"].'" title="'.$parent_cate["parent_name"].'">'.$parent_cate["parent_name"].'</a> &gt;</li>
                          <li>'.$parent_cate["name"].'</li>
                        </ul>
                      </div>';
                    if ($parent_cate["img"]=="") {
                      $lef_html.='<div class="no_images">
                                  <p>No Images</p>
                                  </div>';
                    }else{
                      $lef_html.=' <img class="block_img" src="'.$parent_cate["img"].'" alt="'.$parent_cate["name"].'">';
                    }
  }else{
    if ($parent_cate['slug'] == "learn") {
        $lef_html.= '<div class="block_logo">
                      <img src="'.get_site_url().'/images/learn.svg" alt="LEARN" />
                     </div>
                     <img class="img_design" src="'.get_site_url().'/images/learn_sm.svg" alt="LEARN" />
                     <div class="text_intro">
                      <span class="left"></span>“หากเราไม่เพียงแค่รู้จากสิ่งที่เรียน แต่เราจะเรียนจากสิ่งที่รู้” เราจะเรียนรู้อย่างไร? เพราะหากสมมติฐานนี้เป็นจริง ขอบเขตการเรียนรู้ของเราจะกว้างไกลกว่าเดิมหลายเท่า นั่นเพราะในชีวิตเรามีสิ่งที่ “รู้” ผ่านเข้ามาในชีวิตมากกว่าสิ่งที่ “เรียน” มากมายนัก
                     </div>';
      }else if($parent_cate['slug'] == "journey"){
        $lef_html.= '<div class="block_logo">
                      <img src="'.get_site_url().'/images/journey.svg" alt="JOURNEY" />
                     </div>
                     <img class="img_design" src="'.get_site_url().'/images/journey_sm.svg" alt="JOURNEY" />
                     <div class="text_intro">
                      <span class="left"></span>“หากเราไม่เพียงแค่รู้จากสิ่งที่เรียน แต่เราจะเรียนจากสิ่งที่รู้” เราจะเรียนรู้อย่างไร? เพราะหากสมมติฐานนี้เป็นจริง ขอบเขตการเรียนรู้ของเราจะกว้างไกลกว่าเดิมหลายเท่า นั่นเพราะในชีวิตเรามีสิ่งที่ “รู้” ผ่านเข้ามาในชีวิตมากกว่าสิ่งที่ “เรียน” มากมายนัก
                     </div>';
      }else if($parent_cate['slug'] == "experience"){
        $lef_html.= '<div class="block_logo">
                      <img src="'.get_site_url().'/images/experience.svg" alt="EXPERIENCE" />
                     </div>
                     <img class="img_design" src="'.get_site_url().'/images/experience_sm.svg" alt="EXPERIENCE" />
                     <div class="text_intro">
                      <span class="left"></span>“หากเราไม่เพียงแค่รู้จากสิ่งที่เรียน แต่เราจะเรียนจากสิ่งที่รู้” เราจะเรียนรู้อย่างไร? เพราะหากสมมติฐานนี้เป็นจริง ขอบเขตการเรียนรู้ของเราจะกว้างไกลกว่าเดิมหลายเท่า นั่นเพราะในชีวิตเรามีสิ่งที่ “รู้” ผ่านเข้ามาในชีวิตมากกว่าสิ่งที่ “เรียน” มากมายนัก
                     </div>';
      }else{
        $imgname = '
        <script src="'.get_site_url().'/js/lottie.min.js"></script>
        <div class="canvas_banner" id="container_dc">
            <div id="content_dc"></div>       
            <div id="myborder"></div>   
        </div> 
        <script>
          lottie.loadAnimation({
            container: document.getElementById(\'content_dc\'),
            autoplay: true,
            loop: false,
            renderer: \'svg\',
            animationData: JSON.parse(\'{"v":"5.6.5","fr":30,"ip":0,"op":211,"w":761,"h":424,"nm":"Doctober 2","ddd":0,"assets":[{"id":"image_0","w":92,"h":113,"u":"images/banner_dogtober/","p":"img_0.png","e":0},{"id":"image_1","w":92,"h":113,"u":"images/banner_dogtober/","p":"img_1.png","e":0},{"id":"image_2","w":95,"h":57,"u":"images/banner_dogtober/","p":"img_2.png","e":0},{"id":"image_3","w":72,"h":110,"u":"images/banner_dogtober/","p":"img_3.png","e":0},{"id":"image_4","w":74,"h":113,"u":"images/banner_dogtober/","p":"img_4.png","e":0},{"id":"image_5","w":74,"h":113,"u":"images/banner_dogtober/","p":"img_5.png","e":0},{"id":"image_6","w":72,"h":113,"u":"images/banner_dogtober/","p":"img_6.png","e":0},{"id":"image_7","w":59,"h":25,"u":"images/banner_dogtober/","p":"img_7.png","e":0},{"id":"image_8","w":18,"h":28,"u":"images/banner_dogtober/","p":"img_8.png","e":0},{"id":"image_9","w":761,"h":424,"u":"images/banner_dogtober/","p":"img_9.png","e":0}],"layers":[{"ddd":0,"ind":1,"ty":4,"nm":"Shape Layer 2","td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[380.5,212,0],"ix":2},"a":{"a":0,"k":[0,0,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[-340.5,-212],[-340.5,-99]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 1","np":3,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[-11.75,-3.75],[1,-19.75],[15.75,-4.25],[0,0]],"o":[[0,0],[11.75,3.75],[-1,19.75],[-15.75,4.25],[0,0]],"v":[[-340,-202.5],[-293,-198.75],[-266.75,-153.5],[-294.75,-112.75],[-311,-110]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 2","np":3,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.148],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":7,"s":[0]},{"t":139,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":2,"ix":3,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":300,"st":0,"bm":0},{"ddd":0,"ind":2,"ty":2,"nm":"D-full","tt":1,"refId":"image_0","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[77.293,56.05,0],"ix":2},"a":{"a":0,"k":[45.856,56.299,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ip":0,"op":300,"st":0,"bm":0},{"ddd":0,"ind":3,"ty":4,"nm":"Shape Layer 1","td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[380.5,212,0],"ix":2},"a":{"a":0,"k":[0,0,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[-340.5,-212],[-340.5,-99]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 1","np":3,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[-11.75,-3.75],[1,-19.75],[15.75,-4.25],[0,0]],"o":[[0,0],[11.75,3.75],[-1,19.75],[-15.75,4.25],[0,0]],"v":[[-348,-202.5],[-293,-198.75],[-266.75,-153.5],[-294.75,-112.75],[-353,-109.25]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 2","np":3,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":0,"s":[0]},{"t":84,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":2,"ix":3,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":300,"st":0,"bm":0},{"ddd":0,"ind":4,"ty":2,"nm":"D-Grey","tt":1,"refId":"image_1","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[77.293,56.049,0],"ix":2},"a":{"a":0,"k":[45.856,56.299,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ip":0,"op":300,"st":0,"bm":0},{"ddd":0,"ind":5,"ty":4,"nm":"O Outlines 2","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[296.89,123.597,0],"ix":2},"a":{"a":0,"k":[62.002,64.383,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[-6.784,0.056],[-5.656,3.505],[-3.178,5.889],[6.754,12.455],[5.775,3.556],[11.688,-7.13],[3.186,-5.947],[-0.061,-6.989]],"o":[[5.778,3.55],[6.654,0.065],[5.692,-3.521],[6.708,-12.479],[-3.245,-5.954],[-11.688,-7.13],[-5.732,3.561],[-3.348,6.136],[0,0]],"v":[[-19.665,35.232],[-0.442,40.579],[18.404,35.313],[31.942,20.951],[31.869,-19.001],[18.106,-33.514],[-19.963,-33.514],[-33.576,-19.001],[-38.588,1.013]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0,0,0,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1.991,"ix":5},"lc":1,"lj":1,"ml":10,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[61.582,63.547],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 2","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":1,"k":[{"i":{"x":[0.108],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":8,"s":[100]},{"t":124,"s":[0]}],"ix":1},"e":{"a":0,"k":100,"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":6,"ty":4,"nm":"O Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[296.89,123.597,0],"ix":2},"a":{"a":0,"k":[62.002,64.383,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[-4.962,8.616],[-8.434,5.097],[-17.094,-10.389],[-4.925,-8.593],[10.097,-17.551],[8.476,-4.992],[17.205,10.111]],"o":[[-0.065,-9.943],[4.857,-8.575],[17.093,-10.389],[8.468,5.135],[10.144,17.525],[-4.908,8.527],[-17.177,10.158],[0,0]],"v":[[-56.959,0.177],[-49.48,-28.163],[-29.21,-49.017],[26.433,-49.017],[46.854,-28.087],[46.928,28.594],[26.507,49.219],[-29.214,49.295]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0,0,0,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1.991,"ix":5},"lc":1,"lj":1,"ml":10,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[62.002,64.384],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.24],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":8,"s":[0]},{"t":124,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":7,"ty":4,"nm":"G Shape","td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[380.5,212,0],"ix":2},"a":{"a":0,"k":[0,0,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[121.5,-148.5],[164.25,-148.5]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":19,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 1","np":3,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[4.017,-5.225],[14.755,4.065],[1.5,14]],"o":[[0,0],[0,0],[-5.893,7.665],[-24.5,-6.75],[-1.5,-14]],"v":[[152,-154.75],[151.5,-123.75],[147.733,-114.525],[107,-110],[76,-159.25]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 2","np":3,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.331],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":25,"s":[0]},{"t":141,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":2,"ix":3,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":8,"ty":2,"nm":"G","tt":1,"refId":"image_2","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[495.757,84.127,0],"ix":2},"a":{"a":0,"k":[47.003,28.222,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":9,"ty":4,"nm":"T Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[674.87,173.488,0],"ix":2},"a":{"a":0,"k":[41.405,50.861,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0]],"v":[[13.688,39.5],[13.688,-39.5],[-13.688,-39.5]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0,0,0,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1.991,"ix":5},"lc":1,"lj":1,"ml":10,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[18.665,57.245],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[4.977,0.995],[77.831,0.995]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0,0,0,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1.991,"ix":5},"lc":1,"lj":1,"ml":10,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 2","np":2,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0]],"v":[[14.063,-39.5],[-14.063,-39.5],[-14.063,39.5]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0,0,0,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1.991,"ix":5},"lc":1,"lj":1,"ml":10,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[63.769,57.245],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 3","np":2,"cix":2,"bm":0,"ix":3,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.309],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":34,"s":[0]},{"t":160,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":4,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":12,"ty":4,"nm":"C Outlines 2","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[84.274,235.124,0],"ix":2},"a":{"a":0,"k":[56.916,34.769,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[5.774,3.557],[11.687,-7.13],[3.187,-5.947],[-0.062,-6.99]],"o":[[-3.244,-5.954],[-11.689,-7.13],[-5.732,3.562],[-3.348,6.136],[0,0]],"v":[[35.26,0.814],[21.497,-13.699],[-16.573,-13.699],[-30.186,0.814],[-35.198,20.829]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0,0,0,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1.991,"ix":5},"lc":1,"lj":1,"ml":10,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[58.191,43.731],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 2","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":1,"k":[{"i":{"x":[0.33],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":46,"s":[100]},{"t":119,"s":[0]}],"ix":1},"e":{"a":0,"k":100,"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":13,"ty":4,"nm":"C Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[84.274,235.124,0],"ix":2},"a":{"a":0,"k":[56.916,34.769,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[-4.963,8.616],[-8.433,5.097],[-17.094,-10.389],[-4.925,-8.592]],"o":[[-0.065,-9.942],[4.856,-8.575],[17.093,-10.389],[8.469,5.135],[0,0]],"v":[[-51.874,29.792],[-44.394,1.451],[-24.125,-19.403],[31.519,-19.403],[51.939,1.527]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0,0,0,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1.991,"ix":5},"lc":1,"lj":1,"ml":10,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[56.916,34.769],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.29],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":46,"s":[0]},{"t":119,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":15,"ty":4,"nm":"B Shape","td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[380.5,212,0],"ix":2},"a":{"a":0,"k":[0,0,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[-135,50.5],[-135,163.5]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 1","np":3,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[-7.5,0],[5.385,-13.463],[36,-2.5]],"o":[[0,0],[7.5,0],[-3,7.5],[-27.723,1.925]],"v":[[-145,59.5],[-104,59.5],[-83,78],[-128,104]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 2","np":3,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[-9,-17.5],[-0.5,-9]],"o":[[0,0],[9,17.5],[0.457,8.234]],"v":[[-125.5,106.5],[-78,116.5],[-75,151]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":30,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 3","np":3,"cix":2,"bm":0,"ix":3,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.245],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":55,"s":[0]},{"t":170,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":2,"ix":4,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":16,"ty":2,"nm":"B","tt":1,"refId":"image_3","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[275.496,318.794,0],"ix":2},"a":{"a":0,"k":[35.82,54.702,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":17,"ty":4,"nm":"E Shape 2","td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[380.5,213,0],"ix":2},"a":{"a":0,"k":[0,0,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[77.5,-10.5],[77.5,111.5]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 1","np":3,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[82.75,2.5],[147.25,2.5]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 2","np":3,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[83.5,51.25],[106.625,51.25]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 3","np":3,"cix":2,"bm":0,"ix":3,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[84.25,98.25],[146.25,98.25]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 4","np":3,"cix":2,"bm":0,"ix":4,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.264],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":70,"s":[0]},{"t":194,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":2,"ix":5,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":18,"ty":2,"nm":"E-full","tt":1,"refId":"image_4","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[487.287,263.091,0],"ix":2},"a":{"a":0,"k":[36.75,56.25,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":19,"ty":4,"nm":"E Shape","td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[380.5,213,0],"ix":2},"a":{"a":0,"k":[0,0,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[77.5,-10.5],[77.5,111.5]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":20,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 1","np":3,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[82.75,2.5],[147.25,2.5]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 2","np":3,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[83.5,51.25],[133.5,51.25]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 3","np":3,"cix":2,"bm":0,"ix":3,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[84.25,98.25],[146.25,98.25]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 4","np":3,"cix":2,"bm":0,"ix":4,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.234],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":61,"s":[0]},{"t":165,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":2,"ix":5,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":20,"ty":2,"nm":"E-Grey","tt":1,"refId":"image_5","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[487.287,263.091,0],"ix":2},"a":{"a":0,"k":[36.75,56.25,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":21,"ty":4,"nm":"R Shape","td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[380.5,212,0],"ix":2},"a":{"a":0,"k":[0,0,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[265,83.75],[265,204]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 1","np":3,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[-9.75,-5.25],[-0.75,-8.75],[5.25,-2.75],[3.25,-0.25]],"o":[[0,0],[9.75,5.25],[0.75,8.75],[-5.25,2.75],[-3.25,0.25]],"v":[[272.5,97.75],[309.5,100],[321,127.5],[310.75,145.5],[273.25,153]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":25,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 2","np":3,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[302,143],[302,188.25]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[1,1,1,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":38,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[101.899,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Shape 3","np":3,"cix":2,"bm":0,"ix":3,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.24],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":80,"s":[0]},{"t":201,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":2,"ix":4,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":22,"ty":2,"nm":"R2","tt":1,"refId":"image_6","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[675.318,357.495,0],"ix":2},"a":{"a":0,"k":[35.98,56.299,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":23,"ty":4,"nm":"Layer 12 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[31.142,237.737,0],"ix":2},"a":{"a":0,"k":[0.5,132.62,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[0.5,0.5],[0.5,264.741]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.882000014361,0.882000014361,0.882000014361,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"lc":1,"lj":1,"ml":10,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":38,"s":[0]},{"t":83,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":25,"ty":4,"nm":"Layer 13 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[85.353,322.597,0],"ix":2},"a":{"a":0,"k":[85.662,0.309,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[0.309,0.31],[171.015,0.31]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.882000014361,0.882000014361,0.882000014361,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":0.619,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":60,"s":[0]},{"t":106,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":27,"ty":4,"nm":"Layer 14 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[353.036,158.16,0],"ix":2},"a":{"a":0,"k":[0.5,132.62,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[0.5,0.5],[0.5,264.741]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.882000014361,0.882000014361,0.882000014361,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"lc":1,"lj":1,"ml":10,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":63,"s":[1]},{"t":125,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":29,"ty":4,"nm":"Layer 15 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[277.966,65.535,0],"ix":2},"a":{"a":0,"k":[134.805,0.309,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[0.309,0.309],[269.302,0.309]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.882000014361,0.882000014361,0.882000014361,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":0.619,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":63,"s":[0]},{"t":127,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":31,"ty":4,"nm":"Layer 16 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[651.712,122.754,0],"ix":2},"a":{"a":0,"k":[109.702,0.5,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[0.5,0.5],[218.905,0.5]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.882000014361,0.882000014361,0.882000014361,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":63,"s":[0]},{"t":121,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":33,"ty":4,"nm":"Layer 17 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[711.521,245.296,0],"ix":2},"a":{"a":0,"k":[0.5,178.905,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[0.5,0.5],[0.5,357.31]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.882000014361,0.882000014361,0.882000014361,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"lc":1,"lj":1,"ml":10,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":0,"k":0,"ix":1},"e":{"a":1,"k":[{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":64,"s":[0]},{"t":133,"s":[100]}],"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":35,"ty":4,"nm":"Layer 18 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[688.171,219.373,0],"ix":2},"a":{"a":0,"k":[73.243,0.5,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0]],"o":[[0,0],[0,0]],"v":[[0.5,0.5],[145.987,0.5]],"c":false},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.882000014361,0.882000014361,0.882000014361,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[0,0],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"tm","s":{"a":1,"k":[{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.333],"y":[0]},"t":64,"s":[100]},{"t":123,"s":[0]}],"ix":1},"e":{"a":0,"k":100,"ix":2},"o":{"a":0,"k":0,"ix":3},"m":1,"ix":2,"nm":"Trim Paths 1","mn":"ADBE Vector Filter - Trim","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":37,"ty":4,"nm":"DOCTOBER","td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[380.5,212,0],"ix":2},"a":{"a":0,"k":[0,0,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ty":"rc","d":1,"s":{"a":0,"k":[70.5,34],"ix":2},"p":{"a":0,"k":[0,0],"ix":3},"r":{"a":0,"k":0,"ix":4},"nm":"Rectangle Path 1","mn":"ADBE Vector Shape - Rect","hd":false},{"ty":"fl","c":{"a":0,"k":[1,1,1,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[-205.75,-130],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Rectangle 1","np":3,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":38,"ty":2,"nm":"Dogtober DSN","tt":2,"refId":"image_7","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":1,"k":[{"i":{"x":0.667,"y":1},"o":{"x":0.333,"y":0},"t":61,"s":[172.796,83.158,0],"to":[0,-6.167,0],"ti":[0,6.167,0]},{"i":{"x":0.667,"y":0.667},"o":{"x":0.333,"y":0.333},"t":122.862,"s":[172.796,46.158,0],"to":[0,0,0],"ti":[0,0,0]},{"t":139,"s":[172.796,46.158,0]}],"ix":2},"a":{"a":0,"k":[29.325,12.145,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":39,"ty":2,"nm":"DSN","refId":"image_8","sr":1,"ks":{"o":{"a":1,"k":[{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":75,"s":[0]},{"t":150,"s":[100]}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":1,"k":[{"i":{"x":0.667,"y":1},"o":{"x":0.333,"y":0},"t":75,"s":[430.258,222.221,0],"to":[0,0,0],"ti":[0,0,0]},{"t":150,"s":[438.008,222.221,0]}],"ix":2},"a":{"a":0,"k":[8.652,13.58,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":40,"ty":2,"nm":"Dogtober","refId":"image_9","sr":1,"ks":{"o":{"a":1,"k":[{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":110,"s":[0]},{"t":181,"s":[100]}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":1,"k":[{"i":{"x":0.667,"y":1},"o":{"x":0.333,"y":0},"t":110,"s":[380.5,219.375,0],"to":[0,-0.917,0],"ti":[0,0.917,0]},{"t":181,"s":[380.5,213.875,0]}],"ix":2},"a":{"a":0,"k":[380.5,211.875,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ip":0,"op":294,"st":-6,"bm":0},{"ddd":0,"ind":41,"ty":1,"nm":"White Solid 1","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[380.5,212,0],"ix":2},"a":{"a":0,"k":[380.5,212,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"sw":761,"sh":424,"sc":"#ffffff","ip":0,"op":300,"st":0,"bm":0}],"markers":[]}\'),
          })
          .setSubframe(false)
        </script>
        ';
        $lef_html.= '<div class="block_logo">'.$imgname.'</div>
                     <img class="img_design" src="'.get_site_url().'/images/dogtober_sm.svg" alt="DOGTOBER" />
                     <div class="text_intro">
                      <span class="left"></span>ก่อนที่เราจะเข้าถึงเรื่อง Design Thinking เราต้องมาทำความเข้าใจคำว่า Design หรือการออกแบบ กันใหม่ก่อนนะครับ หลายคนมักมองว่า การออกแบบ คือ การสร้างสิ่งของใหม่ๆ เช่น การสร้างของใช้ การสร้างเฟอร์นิเจอร์ การสร้างบ้าน และการวาดรูป เป็นต้น ใช่ครับนั่นเป็นความเข้าใจที่ถูก แต่เป็นความเข้าใจที่ถูกเพียงส่วนเดียว Simon (1968) ได้กล่าวไว้ว่า การออกแบบ ไม่ได้เป็นแค่การสร้างสิ่งของหรือสิ่งประดิษฐ์ใหม่ๆ เท่านั้น แต่คือความพยายามที่จะเปลี่ยนแปลงสถานการณ์
                     </div>';
      }
  }
  foreach ( $arr_result as $k => $v ) {      
    $res_html.= '<div class="item">';
    $link ="";
    if($template_layout == "search"){
      $link = get_site_url().'/detail/?iii='.$v["id"].'&aaa='.$v['parent_slug'];
      
      $box = "";
      if ($v['parent_name'] == "") {
        $box = "none_active";
      }else{
        $botton = "";
      }
      //var_dump($box);
      $res_html.='<h3><a href="'.$link.'" title="'.$v["name"].'"><span class="txt1 set-txt">'.$v["name"].'</span></a></h3>
                  <div class="item-cate-sign '.$box.'"><a href="'.get_site_url().'/listing/?page='.$v['parent_slug'].'" title="'.$v["parent_name"].'">'.$v["parent_name"].'</a></div>
                  <p><a href="'.$link.'" title="'.$v["name"].'">'.$v["description"].'</a></p>';
    }else{
      if($template_layout == "book" || $template_layout == "home"){
        $link = get_site_url().'/listing/?page='.$v['slug'];
      }else{
        $link = get_site_url().'/detail/?iii='.$v["id"].'&aaa='.$parent_cate['slug'];
      }
      if ($v["img"]=="") {
        $res_html.='<div class="no_images it-category">
          <p>No<br />Images</p>
        </div>';
      }else{
        $res_html.='<div class="img_item it-category">
                      <a href="'.$link.'" title="'.$v["name"].'"><img src="'.$v["img"].'" alt="'.$v["name"].'" /></a>
                    </div>';
      }
      $res_html.='<div class="description it-category">
          <h3><a href="'.$link.'" title="'.$v["name"].'"><span class="txt1 set-txt">'.$v["name"].'</span></a></h3>
          <p><a href="'.$link.'" title="'.$v["name"].'">'.$v["description"].'</a></p>
        </div>';
    }
    $res_html.= '</div>';
  }

  $site_title = $parent_cate["name"];
  if($template_layout == "home"){
    $site_title = "DOGTOBER";
  }else if($template_layout == "search"){
    $site_title = 'ค้นหา '.$txt;
  }else{

  }

  $rs = '';
  $rs.='<input id="set_site_title" type="hidden" value="'.$site_title.'">';
  $rs.='<div class="block_container">';
  	 $rs.= '<div class="block_content">';
  	 	$rs.= '<div class="content_box left">';
  	 		$rs.=' <div class="block_submain">'.$lef_html.'</div>';
  	 	$rs.= '</div>';
  	 	$rs.= '<div class="content_box right">';
  	 		$rs.= '<div class="block_submain">';
  	 			$rs.= '<div class="block_list"><div class="wrap-inn-block_list">'.$res_html.'</div></div>';
  	 		$rs.= '</div>';
  	 	$rs.= '</div>';
  	 $rs.= '</div>';
  $rs.= '</div>';
  return $rs;
}

?>