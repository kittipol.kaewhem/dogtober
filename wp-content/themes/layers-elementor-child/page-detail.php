<?php

  $bbb = isset($_GET["iii"])?$_GET["iii"]:"";
  $active_menu_cc = isset($_GET["aaa"])?$_GET["aaa"]:"";

  $current_cate = get_object_vars(get_category_by_slug($active_menu_cc));
  $active_cate = explode( '//',get_category_parents($current_cate["cat_ID"],false,"//",true),-1);

  if(isset($active_cate[0])){
    $active_menu_cc = $active_cate[0];
  }

  global $wpdb;
  $post = $wpdb->get_results( 

    '
    SELECT
        *
    FROM
        `dogtober_posts`
    WHERE
        ID = "'.$bbb.'"
    ORDER BY
        post_date
    DESC
    ');
  $ep=get_object_vars($post[0]);

  // $wysiwyg = get_field("page_content",$bbb);

  $ttt = str_replace("<!-- wp:nextpage -->", "", $ep["post_content"]);
  $ttt = str_replace("<!-- /wp:nextpage -->", "", $ttt);
  $ttt = str_replace("\n", "", $ttt);
  $ttt = explode("<!--nextpage-->", $ttt);

  $wysiwyg = $ttt;


  global $TITLE;

  $TITLE = $ep["post_title"];

	get_header();

  global $arr_site;

  $args_cate = array('category_name' => $active_menu_cc,'orderby'=>'post_date','order'=>'desc','hide_empty' => false);
  $query_current_cate = new WP_Query($args_cate);
  $arr_id = array();
  $arr_temp = array();
  foreach ($query_current_cate->posts as $key => $value) {
    $arr_temp[]=$value->ID;
  }
  
  for ($i=count($arr_temp)-1; $i >= 0; $i--) { 
    $arr_id[] = $arr_temp[$i];
  }
  // var_dump($arr_temp,$arr_id);

  $index_page = array_search($bbb, $arr_id);


  // current sub_cate and current group
  $cc_subcate_id = "";
  $cc_group_id = "";
  $cc_parent_id = $query_current_cate->query_vars["cat"];
  $all_subcate_and_group_id = array(); 

  $arr_cate_list = get_the_category($ep["ID"]);
  $cate_list = array();
  $parent_cate = array();

  foreach ($arr_cate_list as $key => $value) {
    $cate_list[] = get_object_vars($value);
  }

  function cmp($a, $b) {
      return strcmp($a["parent"], $b["parent"]);
  }
  usort($cate_list, "cmp");

  foreach ($cate_list as $key => $value) {
    $parent_cate[] =  $value["slug"];
  }

  // Create Index list
  $index_id = 0;
  if(count($cate_list)>0){
    $index_id = $cate_list[count($cate_list)-1]["term_id"];
  }
  $args = array(
  'category'         => $index_id,
  'numberposts'      => '-1',
  'orderby'          => 'post_date',
  'order'            => 'DESC',
  ); 
  $index_term = get_object_vars(get_term($index_id));
  $index_list = get_posts($args);
?>
<input id="set_site_title" type="hidden" value="<?=$ep["post_title"]?>">
<div class="block_container">
	<div class="block_content content_detail">
    <input type="hidden" id="current_page" value="1">
    <input type="hidden" id="max_page" value="<?=count($wysiwyg);?>">
	 	<div class="content_box left">
	 		 <div class="block_submain">
	 			<div class="subject"><?=$ep["post_title"]?></div>
        <div class="block_category">
          <ul>
            <?php
                $breadcrumb = '';
                foreach ($parent_cate as $k => $v) {
                  if($k<2){
                    $cv = get_object_vars(get_category_by_slug($v));
                    $breadcrumb.='<li><a href="'.get_site_url().'/listing/?page='.$cv["slug"].'">'.$cv["name"].'</a></li> > ';
                  }
                }
            ?>
            <?=$breadcrumb?><li><?=$ep["post_title"]?></li>
          </ul>
        </div>
        <?php 
          if(get_the_post_thumbnail_url($ep['ID'])!=""){
        ?>
	 			 <img class="block_img" src="<?php echo get_the_post_thumbnail_url($ep['ID']) ?>" alt="DOGTOBER" />
        <?php } ?>
	 		</div>
	 	</div>
	 	<div class="content_box right">
	 		<div class="block_submain">
        <?php foreach ($wysiwyg as $k => $v) { 
            $p_index=$k+1;
          ?>
	 			<div class="block_list block_detail block_active block_active<?=$p_index?> <?php if ($k=='0') {echo 'active';}?>" data-ppp="<?=$p_index?>" data-iii="<?=$bbb?>">
              <div class="wysiwyg-cotent">
                <?=$v?> 
              </div>
	 			</div>
        <?php } ?>
	 		</div>
      <div class="block_pagebook">
        <div class="bp-block page-ctrl">
          <div class="block_bookmask inline">
            <i class="fas fa-bookmark icon_bookmask"></i>
          </div>
          <div class="block_sound inline sound-off">
            <i class="fas fa-volume-up vol-sign vol-on"></i><i class="fas fa-volume-mute vol-sign vol-off"></i>
             <audio id="bgm_1" loop>
              <source src="<?=get_site_url()?>/sound/song_of_sadhana.mp3" type="audio/mp3">
              Your browser does not support the audio element.
            </audio> 
          </div>
        </div>
        <div class="bp-block">
          <div class="block_pagination">
            <ul>
              <?php
                $prev_link = "";
                $next_link = "";
                $close_book = false;
                if($index_page>=0){
                  if($index_page==0){
                    if(isset($arr_id[1])){
                      $next_link = get_site_url().'/detail/?iii='.$arr_id[$index_page+1].'&aaa='.$active_menu_cc;
                    }else{
                      $close_book = true;
                      $next_link = get_site_url().'/listing/?page='.$active_menu_cc;
                    }
                  }else if($index_page != count($arr_id)-1){
                    $prev_link = $next_link = get_site_url().'/detail/?iii='.$arr_id[$index_page-1].'&aaa='.$active_menu_cc;
                    if(isset($arr_id[$index_page+1])){
                      $next_link = get_site_url().'/detail/?iii='.$arr_id[$index_page+1].'&aaa='.$active_menu_cc;
                    }
                  }else{
                    $close_book = true;
                    $prev_link = $next_link = get_site_url().'/detail/?iii='.$arr_id[$index_page-1].'&aaa='.$active_menu_cc;
                    $next_link = get_site_url().'/listing/?page='.$active_menu_cc;
                  }
                }
              ?>
               <?php $max_page = count($wysiwyg);
              $botton = "";
                if ($max_page <= 1) {
                  $botton = "none_active";
                }else{
                  $botton = "";
                }
                //var_dump($botton);
               ?>
              <li class="pgn pagination_left <?=$botton?>">
                <a data-direction="prev" class="button_prev conf_button" <?php if($close_book){ echo 'class= "close-book" ';}?> title="หน้าก่อนหน้า">
                  <svg class="ico_arrow" xmlns="http://www.w3.org/2000/svg" width="24.858" height="14.234" viewBox="0 0 24.858 14.234">
                    <path id="path" d="M6.813,4,5.568,5.245l4.982,4.982H-10.928v1.779H10.55L5.568,16.989l1.245,1.245,7.117-7.117Z" transform="translate(13.93 18.234) rotate(180)" fill="#333" fill-rule="evenodd"/>
                  </svg><span class="pgn-page-text">หน้าก่อนหน้า</span>
                </a>
              </li>
              <li class="pgn-number"><span class="no_page">1</span>/<?=count($wysiwyg);?></li>
              <li class="pgn pagination_right <?=$botton?>">
                <a data-direction="next" class="button_next conf_button active" <?php if($close_book){ echo 'class= "close-book" ';} ?> title="หน้าถัดไป">
                  <span class="pgn-page-text">หน้าถัดไป</span><svg class="ico_arrow flip" xmlns="http://www.w3.org/2000/svg" width="24.858" height="14.234" viewBox="0 0 24.858 14.234">
                    <path id="path" d="M6.813,4,5.568,5.245l4.982,4.982H-10.928v1.779H10.55L5.568,16.989l1.245,1.245,7.117-7.117Z" transform="translate(13.93 18.234) rotate(180)" fill="#333" fill-rule="evenodd"/>
                  </svg>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
	 	</div>
  </div>
</div>
<?php get_footer(); ?>
<div class="block_all_content">
  <div class="close"></div>
  <div class="list">
    <div class="wrap-list">
      <div class="head">
        <i class="fas fa-bookmark icon_bookmask"></i>
        <span class="head_text">สารบัญ</span>
        <span class="icon_return"><i class="fas fa-angle-right"></i></span>
      </div>
      <div class="search">
        <input type="text" class="cs-field" name="txtsearch" placeholder="ค้นหาชื่อบทความ" />
      </div>
      <div class="block_category">
        <?php
        foreach ($index_list as $k => $v) {
          $ev = get_object_vars($v);
          $link = get_site_url().'/detail/?iii='.$ev["ID"].'&aaa='.$index_term['slug'];
        ?>
        <div class="category">
          <a href="<?=$link?>"><?=$ev["post_title"]?></a>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>

