(function($){
	$.fn.serializeObject = function()
	{
	   var o = {};
	   var a = this.serializeArray();
	   $.each(a, function() {
	       if (o[this.name]) {
	           if (!o[this.name].push) {
	               o[this.name] = [o[this.name]];
	           }
	           o[this.name].push(this.value || '');
	       } else {
	           o[this.name] = this.value || '';
	       }
	   });
	   return o;
	};
	$.fn.gotoAnchor = function(opts){
		var hd;
		if($(window).width() > 767){
			hd = -146;
		}else{
			hd = -70;
		}
		if( $(this).length>0 ){
			var defaults={
				speed:'slow',
				stop:false,
				offsetTop: ($("#header").length > 0)?$("#header")[0].offsetHeight+516:0,
				anotherTop: hd,
				callBack:function(){}
			};
			$.extend(true,defaults,opts);
			if (!defaults.stop) {
				$('html,body').animate({scrollTop:$(this).offset().top+defaults.anotherTop},defaults.speed).promise().done(function(){defaults.callBack();});
		   	}else{
		   		$('html,body').stop(true,false).animate({scrollTop:$(this).offset().top+defaults.anotherTop },defaults.speed).promise().done(function(){defaults.callBack();});
		   	}
		}
	}

	// Document Ready
	$(document).ready(function(){
		document.title = $("#set_site_title").val();
		$("#popup-page.popup .btn-close,#popup-page.popup .no,#alert-page.popup .btn-close,#alert-page.popup .no,#popup-page.popup .dim").click(function(){
			setDefaultPopup();
		});
		var scroll_class = ".block_list";
		$(scroll_class).niceScroll({
			cursorcolor:"#dddddd",
			cursorfixedheight: 70,
			cursorwidth:"3"
		});
		$(".block_bookmask").click(function(e) {
			e.preventDefault();
			$(".block_all_content").addClass("active");
			return false;
		});
		$( ".block_all_content a").click(function(e) {
			e.preventDefault();
			document.location.href = $(this).attr("href");
			return false;
		});
		$( ".block_all_content .close").click(function(e) {
			e.preventDefault();
			$(this).parent().removeClass("active");
			return false;
		});
		$(".block_all_content .icon_return").click(function(e) {
			e.preventDefault();
			$(this).parents(".block_all_content").removeClass("active");
			return false;
		});
		$(".header-search .cs-sign").click(function(){
			$(".custom-search").addClass("active");
			$(".custom-search .cs-field").focus();
		});
		$("input.cs-field").keyup(function(e){
		    if(e.keyCode == 13)
		    {
		        var kw = $(this).val();
		        document.location.href = base_url+'/search/?kw='+kw;
		    }
		});
		$(".cs-submit").click(function(){
			var kw = $(".cs-field2").val();
	        document.location.href = base_url+'/search/?kw='+kw;
		});
		$(".custom-search .cs-close").click(function(){
			$(".custom-search").removeClass("active");
		});
		//$(document).click(function(){
		//	$(".block_all_content").removeClass("active");
		//});
		$(".content_detail #current_page").val(1)
		$( ".content_detail .conf_button" ).click(function() {
			var direction = $(this).attr("data-direction");
  			// alert(direction);
  			var current_page = parseInt($("#current_page").val());
  			var max_page = $("#max_page").val();
  			// console.log(current_page,max_page,direction);
  			$(".block_active").removeClass("active");

  			if (direction=="next") {
  				current_page=current_page+1;
  				$(".button_prev").addClass("active");
  				if (current_page==max_page) {
  					$(".button_next").removeClass("active");
  				}else{
  					$(".button_next").addClass("active");
  				}
  			}else{
  				current_page=current_page-1;
  				$(".button_next").addClass("active");
  				if (current_page==1) {
  					$(".button_prev").removeClass("active");
  					
  				}else{
  					$(".button_prev").addClass("active");
  				}
  			}
  			// console.log(current_page,max_page,direction);
  			// console.log(".block_active"+current_page);
  			$(".block_active"+current_page).addClass("active");
  			$("#current_page").val(current_page);
  			$(".no_page").text(current_page);
		});
		bgm = document.getElementById("bgm_1");
		// bgm.play();
		$(".block_sound").click(function(){
			if($(this).hasClass("sound-on")){
				$(this).removeClass("sound-on");
				$(this).addClass("sound-off");
				bgm.pause();
			}else{
				$(this).addClass("sound-on");
				$(this).removeClass("sound-off");
				bgm.play();
			}
		});
	});
	// Document Ready

	function setDefaultPopup(){
		$(".popup").hide();
		$("#popup-page.popup .p-content").html("");
	}
	function YouTubeGetID(url){
	  var ID = '';
	  url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
	  if(url[2] !== undefined) {
	    ID = url[2].split(/[^0-9a-z_\-]/i);
	    ID = ID[0];
	  }
	  else {
	    ID = url;
	  }
	    return ID;
	}

})(jQuery);

// ====== อย่าลบ 2020 04 10 ======

// function() {
//   var s_input = $(this).val();
//   if (s_input != "") {
//     $.each($('.obj-item'), function(i, e) {
//       var data = $(e).data('search').toLowerCase();
//       if (data.indexOf(s_input.toLowerCase()) == -1)
//         $(e).fadeOut();
//       else
//         $(e).fadeIn();
//     });
//   } else {
//     $('.obj-item').fadeIn();
//   }
// }

// ====== อย่าลบ 2020 04 10 ======
